import { Component } from '@angular/core';
import { Post } from './post';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'blog-angular';

  postArray = [
    new Post('Mon premier post',
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi neque magna, vulputate vitae sem a, ultricies efficitur augue. Ut in venenatis eros. Vivamus mattis libero.',
      0,
      new Date()
    ),
    new Post('Mon deuxième post',
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi neque magna, vulputate vitae sem a, ultricies efficitur augue. Ut in venenatis eros. Vivamus mattis libero.',
      0,
      new Date()
    ),
    new Post('Encore un post',
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ornare semper commodo. Pellentesque eu mauris pharetra, condimentum nisl id, dapibus lorem. Maecenas aliquam nisi sit amet varius aliquet. Maecenas sapien.',
      0,
      new Date()
    )
  ]
}
